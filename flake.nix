{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  outputs = {self, nixpkgs}:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    packages.${system}.default = pkgs.buildEnv {
      name = "website";
      paths = with pkgs; [
        hugo
      ];
    };
  };
}
