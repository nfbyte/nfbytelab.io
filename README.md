### Development
#### Requirements
- `git`
- `nix`
#### Instructions
1. Download:
```sh
git clone https://gitlab.com/nfbyte/nfbyte.gitlab.io .
```
2. Make sure git submodules are checked out:
```sh
git submodule update --init
```
3. Apply design tweaks:
```sh
git apply design.patch
```
4. Install build dependencies:
```sh
nix profile install
```
5. Build:
```sh
nix develop --command hugo
```
