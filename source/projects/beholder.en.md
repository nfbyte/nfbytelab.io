---
title: Beholder
date: 2022-12-01
---

### Introduction
Beholder is a minimal and secure Linux desktop workstation based on [Alpine Linux](https://alpinelinux.org) suitable for software development and operations.

### Features
- Encrypted DNS ([DoT](https://en.wikipedia.org/wiki/DNS_over_TLS) + [DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions))
- Keyboard-driven interface ([Sway](https://en.wikipedia.org/wiki/Sway_(window_manager)))
- Web browser ([firefox](https://mozilla.org/firefox/new))

* * *

![](/static/beholder/screenshot-1.png)
![](/static/beholder/screenshot-2.png)

* * *

### Installation

> Requirements:
>
> A physical desktop computer with an Ethernet connection, a single storage drive, a display and a keyboard.

Download and boot [Alpine Linux 3.17.0](https://dl-cdn.alpinelinux.org/alpine/v3.17/releases/x86_64/alpine-standard-3.17.0-x86_64.iso) (assuming `amd64`).

Then, once booted:
* * *
1. Setup live environment (log in as the `root` user)
```
setup-alpine
```
```
poweroff
```
* * *
### Post-Installation
13. Set root password
```
passwd
```
* * *
14. Add a regular user
```
adduser user
```
```
passwd user
```
```
addgroup user input
```
```
addgroup user video
```
```
addgroup user audio
```
```
addgroup user wheel
```
```
addgroup user kvm
```
```
apk add doas
```
```
vi /etc/doas.d/doas.conf
```
```txt
permit nopass user
```
* * *
15. Set hostname
```
vi /etc/hostname
```
```txt
beholder
```
* * *
16. Set keymap
```
setup-keymap
```
* * *
17. Set timezone
```
setup-timezone
```
* * *
18. Set up networking
```
setup-interfaces
```
* * *
19. Setup system clock
```
setup-ntp
```
* * *
### DNS
```
apk add ca-certificates unbound
```
```
install -d -o unbound -g unbound -m 700 /var/lib/unbound
```
```
doas -u unbound unbound-anchor -a /var/lib/unbound/root.key
```
```
vi /etc/unbound/unbound.conf
```
```txt
server:
  tls-upstream: yes
  tls-cert-bundle: "/etc/ssl/certs/ca-certificates.crt"
  auto-trust-anchor-file: "/var/lib/unbound/root.key"
forward-zone:
  name: "."
  forward-addr: 8.8.8.8@853#dns.google
```
```
rc-update add unbound
```
```
rc-service unbound start
```
```
vi /etc/resolv.conf
```
```txt
nameserver 127.0.0.1
```
* * *
### Display
```
apk add mesa-dri-gallium libudev-zero seatd nerd-fonts sway foot zsh vim
```
```
setup-devd mdev
```
```
addgroup user seat
```
```
rc-update add seatd
```
```
rc-service seatd start
```
```
vi /etc/profile.d/xdg_runtime_dir.sh
```
```sh
if test -z "${XDG_RUNTIME_DIR}"; then
  export XDG_RUNTIME_DIR=/tmp/$(id -u)-runtime-dir
  if ! test -d "${XDG_RUNTIME_DIR}"; then
    mkdir "${XDG_RUNTIME_DIR}"
    chmod 0700 "${XDG_RUNTIME_DIR}"
  fi
fi
```
```
sway
```
* * *
### Sound (optional)
```
apk add alsa-lib alsa-utils alsa-ucm-conf
```
* * *
### Further
```
apk add firefox
```
