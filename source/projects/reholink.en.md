---
title: Reholink
date: 2020-12-21
---

### Introduction
Reholink is a Linux server configuration based on [Ubuntu Server](https://ubuntu.com/server) 20.04, serving as a companion to [Cloudberry](/projects/cloudberry) for the primary purpose of data storage and archiving using the [ZFS](https://en.wikipedia.org/wiki/ZFS) storage system. Additionally, it provides local network-level ad-blocking and serves as a media server.

### Applications
- [AdGuard Home](https://adguard.com/en/adguard-home/overview.html)
- [File Browser](https://filebrowser.org/features)
- [Emby](https://emby.media)

### Installation
Installation requires an `amd64` computer (connected to the internet via Ethernet cable) with a monitor (only necessary during the setup, after which it can be permanently removed) and at least 20GB of available storage. Download [Ubuntu Server 20.04 LTS](https://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-live-server-amd64.iso), flash it to an USB drive and install it on the computer. Choose a username (referred to as `$user`), a hostname (referred to as `$host`) and disable automatic system updates.
* * *
The following script will set up everything:
```shell
{{<file "/static/reholink/install.sh" >}}
```
It can be downloaded and run on the server directly:
```
curl https://nfbyte.srht.site/static/reholink/install.sh | sudo sh
```
To enable ad-blocking, set the server's IPv4 address (referred to as `$ip`) as the DNS server in the local network's router / access point settings. It can be found with:
```
hostname --ip-address
```
The applications will be reachable on the local network at:
- AdGuard Home: `http://$ip:3000`
- File Browser: `http://$ip:4000`
- Emby: `http://$ip:7000`

ZFS configuration depends on the storage hardware setup. Storage devices can be identified with:
```
lsblk
```
A minimal [RAID-Z1](https://en.wikipedia.org/wiki/Non-standard_RAID_levels#RAID-Z) array can be created with:
``` 
zpool create Files raidz1 /dev/$device1 /dev/$device2 /dev/$device3
```

SSH (using public key authentication) can be used for access from a remote system.

To generate a private/public key pair:
```
ssh-keygen
```
To transfer the public key from the remote system to the `$host`:
```
ssh-copy-id $user@$host
```
```
ssh -t $user@$host sudo cp --recursive .ssh /root
```

[ZeroTier](https://www.zerotier.com) can be used for remote access from outside the local network.

* * *

### Maintenance

- Check system status
```
systemctl status
```
- Download system updates
```
apt update && apt upgrade && apt autoremove
```
- Apply system updates
```
systemctl reboot
```
- Download application updates
```
docker compose --file /etc/docker/compose.yml pull
```
- Apply application updates
```
docker compose --file /etc/docker/compose.yml up --detach
```
- Check storage status
```
zpool status
```
- Perform routine [data scrubbing](https://en.wikipedia.org/wiki/Data_scrubbing)
```
zpool scrub Files
```
