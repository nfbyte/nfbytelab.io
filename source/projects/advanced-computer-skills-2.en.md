---
title: <Guide> Advanced Computer Skills - Part 2
date: 2021-10-08
---

## Building an Operating System

* * *

### Introduction

An operating system can be built from software components in the same way that a PC can be built from hardware components. The most important component of an OS is its kernel, which is a program that interfaces with a computer's hardware and acts as a platform for other programs to run on. The only available kernel with modern hardware support is [Linux](https://en.wikipedia.org/wiki/Linux_kernel).

* * *

### Software
A complete desktop operating system is composed of the following main components:

> ---
> - Kernel - [Linux](https://kernel.org/linux.html)
> - System Toolset - [systemd](https://systemd.io)
> - Multimedia Server - [PipeWire](https://pipewire.org)
> - Desktop Environment - [Plasma](https://kde.org/plasma-desktop)
> - Application Manager - [Flatpak](https://flatpak.org)
> ---

* * *

### Arch Linux

Linux distributions (distros) are software update services featuring repositories of continuously maintained packages.

[Arch Linux](https://wiki.archlinux.org/title/Arch_Linux) is the most well-suited distro for a personal desktop OS. It's designed around [systemd](https://wiki.archlinux.org/title/systemd) and [pacman](https://wiki.archlinux.org/title/pacman) (a [package manager](https://en.wikipedia.org/wiki/Package_manager)).

The Arch Linux `iso` can be [downloaded](https://archlinux.org/download), and flashed (i.e. copied) on a USB flash drive (the flash drive must have a sufficient storage capacity to support the `iso`) using [various methods](https://wiki.archlinux.org/title/USB_flash_installation_medium).

Once the flash is complete, the computer (on which the OS is being installed) must boot from the USB drive in order to begin the installation. The system must be powered off and the USB drive must be plugged into it; the boot device can be specified in the computer's firmware - the firmware menu can be accessed by pressing a specific key on the keyboard during the system's startup process. Unfortunately, the key is not standardized, but it's usually Escape, Delete, F2 or F12.

* * *

### Pre-Installation
A guided installation procedure can be started by running the command:
```
archinstall
```

#### Boot Method
The simplest and most straight forward boot configuration for a single operating system is to use [Unified Kernel Images](https://wiki.archlinux.org/title/Unified_kernel_image) with no bootloader (i.e. the [`EFISTUB`](https://wiki.archlinux.org/title/EFISTUB) bootloader).

#### Filesystems
Filesystems in Unix-derived systems (such as Linux-based operating systems) are not only used for file storage, but also act as an interface for the kernel, representing kernel functionality as files (devices, processes, etc.).

The simplest filesystem for a single NVMe drive is [ext4](https://en.wikipedia.org/wiki/Ext4).

#### Users
User accounts in [Unix](https://en.wikipedia.org/wiki/Unix)-derived systems are a security mechanism and don't necessarily refer to human users. The default privileged user account used for system administration is `root`. For the default unprivileged user account, the name `user` can be used.

User account setup can be skipped; it will be done in the [Post-Installation](#post-installation) section.

#### Additional packages
A useful package for editing configuration files (used in the next section) is `nano`.

* * *

### Post-Installation

#### Networking
The first requirement for internet connectivity is handled by [systemd-networkd](https://wiki.archlinux.org/title/Systemd-networkd).

The service must be enabled (so it starts automatically on boot):
```
systemctl enable systemd-networkd
```
Network setup can be done using a [network file](https://wiki.archlinux.org/title/Systemd-networkd#network_files). When using a wired internet connection, it makes more sense to set up a static internal IPv4 address than to use [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol).

Example configuration:
```
nano /etc/systemd/network/20-wired.network
```
```
[Match]
Name=enp1s0 # network interface name

[Network]
Gateway=192.169.100.1 # gateway (router) IPv4 address
Address=192.168.100.2/24
```

The second requirement ([DNS](https://en.wikipedia.org/wiki/Domain_Name_System) resolution) is handled by [systemd-resolved](https://wiki.archlinux.org/title/Systemd-resolved):
```
systemctl enable systemd-resolved
```
Example configuration:
```
nano /etc/systemd/resolved.conf
```
```
[Resolve]
DNS=9.9.9.9#dns.quad9.net
DNSOverTLS=yes
```

#### Home Directory
Encrypted home directories for personal data are handled by [systemd-homed](https://wiki.archlinux.org/title/systemd-homed):
```
systemctl enable --now systemd-homed
```
The home directory itself (along with the user `user`) must be created:
```
homectl create user --storage=luks --fs-type=ext4 --disk-size=90%
```
All data within the home directory will be stored in a single file (`/home/user.home`) and the home directory (`/home/user`) will be available after logging in as `user`.

#### Multimedia
Multimedia streams (audio & video) are handled by [PipeWire](https://en.wikipedia.org/wiki/PipeWire), which must be installed:
```
pacman --sync pipewire
```
To function, PipeWire requires a session manager such as [WirePlumber](https://wiki.archlinux.org/title/WirePlumber):
```
pacman --sync wireplumber
```

#### Bluetooth
[Bluetooth](https://wiki.archlinux.org/title/bluetooth) support can be added like so:
```
pacman --sync bluez
```
```
systemctl enable bluetooth
```

#### Desktop Environment
The main components of the [Plasma](https://en.wikipedia.org/wiki/Plasma_(software)) desktop environment can be installed like so:
```
pacman --sync plasma-desktop plasma-pa
```
Some basic applications such as a [terminal emulator](https://en.wikipedia.org/wiki/Terminal_emulator) ([Konsole](https://en.wikipedia.org/wiki/Konsole)) and [file manager](https://en.wikipedia.org/wiki/File_manager) ([Dolphin](https://en.wikipedia.org/wiki/Dolphin_(file_manager))) will be necessary:
```
pacman --sync konsole dolphin
```
The desktop environment is started by a display manager ([SDDM](https://wiki.archlinux.org/title/SDDM)):
```
pacman --sync sddm sddm-kcm sddm-theme-breeze
```
Required configuration:
```
nano /etc/sddm.conf.d/10-wayland.conf
```
```
[General]
DisplayServer=wayland
GreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell

[Wayland]
CompositorCommand=kwin_wayland --drm --no-lockscreen --no-global-shortcuts --locale1
```
Once SDDM is enabled, the next boot will present a graphical login screen.
```
systemctl enable sddm
```
An application manager such as [Flatpak](https://en.wikipedia.org/wiki/Flatpak) allows desktop applications to be installed and work in the same way across all distros:
```
pacman --sync flatpak flatpak-kcm xdg-desktop-portal-kde
```
To finish system configuration and boot into the desktop environment:
```
systemctl reboot
```

* * *

### Applications
Flatpak applications (flatpaks) can be installed from Flatpak repositories.

[Flathub](https://flathub.org) is the main repository of community-maintained flatpaks available:
```
flatpak --user remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
```
All that is needed to install a flatpak from Flathub is its Application ID (in the form `com.example.application`):
```
flatpak --user install com.example.application
```
Alternatively, a flatpak can be installed without specifying a repository from a `flatpakref` file.  Once it's installed, it should appear in Plasma's application menu.

Flatpaks are isolated from the system, and may sometimes need explicit permissions granted to access certain resources such as filesystem paths or special hardware devices (e.g. a webcam).

To update all installed flatpaks:
```
flatpak --user update
```

* * *

### Maintenance
System packages are updated regularly by package maintainers. To update all packages to the latest versions:
```
run0 pacman --sync --refresh --sysupgrade
```

* * *

### Games
Since the vast majority of modern computer games are Microsoft Windows applications, [Wine](https://www.winehq.org) (an implementation of the [Windows API](https://en.wikipedia.org/wiki/Windows_API)) is required:
```
run0 pacman --sync wine
```

Once installed, running a game (with `game.exe` being the game's main executable file) is as simple as running `wine game.exe` within the game's directory.

For finding more technical information about games, [PCGamingWiki](https://www.pcgamingwiki.com/wiki/Home) can be a useful website.

* * *

### Beyond

The [ArchWiki](https://wiki.archlinux.org) is an invaluable resource, however be wary that many parts of the Linux software ecosystem are in constant and rapid change - articles which have not been updated over time can provide outdated and misleading information.
