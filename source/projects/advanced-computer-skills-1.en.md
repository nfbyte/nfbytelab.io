---
title: <Guide> Advanced Computer Skills - Part 1
date: 2021-10-07
---

## Building a Personal Computer

* * *

### Introduction
Building a PC from off-the-shelf components is not a task well suited for users who want a computer that works out-of-the-box (i.e. the vast majority of people). However, it is the most practical beginner project for any prospective computer technician.

Final assembly is straight forward and will not be covered here since there are plenty of tutorials for that readily available on the web. The tricky part is making informed choices about which components to use. Unfortunately, the computer hardware retail industry is a complete disaster, filled with lies, deception, and misleading marketing practices.

* * *

### Hardware
To build a complete PC from scratch, the following components are necessary:
> ---
> - [Processor](#processor)
> - [Graphics Card](#graphics-card) (optional)
> - [Random-Access Memory](#random-access-memory)
> - [Non-Volatile Memory](#non-volatile-memory)
> - [Motherboard](#motherboard)
> - [Power Supply](#power-supply)
> - [Case](#case)
> - [Monitor](#monitor)
> - [Peripherals](#peripherals) (Keyboard, Mouse, Webcam, Speakers, etc.)
> ---

Not all components are compatible with each other. The main limiting compatibility factor is between the processor and motherboard. Processors are produced in generations (or series), with each new generation typically more performant than the previous. A motherboard's processor socket determines which generations of processors are compatible with it.

Processors are typically referred to as central processing units ([CPU](https://en.wikipedia.org/wiki/Central_processing_unit)s), but they additionally include an integrated graphics processing unit ([GPU](https://en.wikipedia.org/wiki/Graphics_processing_unit)) which makes a dedicated graphics card entirely optional.

Additionally, two main types of memory are needed for storing data: volatile and non-volatile. Random-access memory ([RAM](https://en.wikipedia.org/wiki/Random-access_memory)) is fast but volatile - it requires power to maintain the stored data. To store data that persists even when the computer is powered off, non-volatile memory is required. This is where the software will be installed.

> #### Protip
> Directly comparing different components based on specifications alone is a non-trivial task; the most effective way to gauge performance is through benchmarks.

* * *

### Processor
The processor is the most important component and as such should be chosen first.

Desktop processors use the [`x86_64`](https://en.wikipedia.org/wiki/X86-64) (a.k.a. `amd64`) instruction set architecture ([ISA](https://en.wikipedia.org/wiki/Instruction_set_architecture)).

There are two main `amd64` processor manufacturers: Intel and AMD, each with their own main brand for consumer desktop processors: Core i3/i5/i7/i9 for Intel and Ryzen 3/5/7/9 for AMD.

{{<figure src="/static/advanced-computer-skills-1/intel.png" class="center" >}}

> "Unlocked" Intel processors do not come with a stock cooler and will need a separate cooling solution, but can be overclocked (i.e. their operating frequency can be increased).

The primary way in which processors improve between generations is through advancements in the manufacturing method, known as process technology. One might see this marketed through a nanometer count (e.g. 7 nanometers), however this is yet another branding term and thus is mostly meaningless.

In order to facilitate parallel computation, modern processors have multiple physical compute units (cores), and feature hardware multithreading (branded as Simultaneous Multithreading for AMD and Hyper-Threading for Intel) - i.e. one core can provide two logical processors ([threads](https://en.wikipedia.org/wiki/Thread_(computing))).

* * *

### Graphics Card
Graphics cards (primarily useful for accelerating either 3D applications or machine learning applications) are expansion cards that attach to the motherboard through Peripheral Component Interconnect Express ([PCIe](https://en.wikipedia.org/wiki/PCI_Express)) slots.

The two main graphics card manufacturers are Nvidia and AMD, with respective consumer graphics card brands: GeForce for Nvidia and Radeon for AMD.

> #### Protip
> Out of the two main graphics card manufacturers, only AMD offers drivers with full Linux support (see [Building an Operating System](/projects/guide-advanced-computer-skills-part-2)). Given this, it's best to just avoid Nvidia graphics cards entirely.

* * *

### Random-Access Memory
The processor socket also determines RAM compatibility, as there are multiple types (e.g. DDR3, DDR4, DDR5, HBM, etc.).

Desktop processors support dual-channel memory (i.e. using two RAM sticks in parallel); on motherboards which have more than two RAM slots, any extra RAM sticks will only serve to increase total memory capacity and will make no difference in performance.

* * *

### Non-Volatile Memory
The current standard specification for non-volatile memory is Non-Volatile Memory Express ([NVMe](https://en.wikipedia.org/wiki/NVM_Express)). Most NVMe drives attach to the motherboard through the M.2 slot, which is a special type of PCIe slot.

* * *

### Motherboard
Other than the processor socket being compatible with the desired processor, the only thing to consider when choosing a motherboard is the form factor (e.g. ATX, microATX, Mini-ITX), which determines case compatibility. Optionally, some motherboards support Wi-Fi, however, for a stationary PC, connecting to the internet via an Ethernet cable is preferred. All semi-modern motherboards designed for `amd64` processors support the [UEFI](https://en.wikipedia.org/wiki/UEFI) specification.

* * *

### Power Supply
The power supply unit ([PSU](https://en.wikipedia.org/wiki/Power_supply_unit_(computer))) provides electric power for the other components. A PSU's wattage is the measure in watts (`W`) of the maximum power it can provide, so one must make sure to choose a PSU with a wattage that exceeds the total power requirement of all components. The latter can be calculated using the `Power = Voltage * Current` formula, where `Voltage` is measured in volts (`V`) and `Current` in amperes (`A`).

PSU efficiency should be considered - if a PSU has 50% efficiency, in order to provide 50W of power it will draw 100W in total. PSUs that use the "80 Plus" rating guarantee at least 80% efficiency across 20%, 50% and 100% power loads.

* * *

### Case
The case must be compatible with the motherboard form factor. Case fans are necessary for air cooled PCs, and can be configured such that the air pressure inside the case is positive (i.e. higher than on the outside) in order to prevent dust build-up. This is achieved by simply using more intake fans (fans that draw air into the case) than exhaust fans (fans that push air out of the case).

* * *

### Monitor
There are several important factors to consider when choosing a monitor:

#### Aspect Ratio
The aspect ratio is the ratio between screen width (`W`) and height (`H`). `16:9` is the current standard and is what most digital content is available in. Other common aspect ratios are `4:3` (i.e. `12:9` - which is narrower) and `21:9` (which is wider). `4:3` is the previous standard and as such a lot of legacy content remains bound to it. `21:9` is in fact a branding term for monitors using weird aspect ratios that are roughly equivalent to `2.39:1`, which is the aspect ratio of most movies.

#### Resolution
A monitor's resolution (i.e. hardware resolution) is its highest supported software resolution - which is the total amount of [pixels](https://en.wikipedia.org/wiki/Pixel) of an image. Using `W` as the width in pixels and `H` as the height in pixels, software resolution is represented either by itself as `W x H` (e.g. `1920 x 1080`) or together with the aspect ratio as `Hp` (e.g. `16:9 1080p`).

For a given hardware resolution, images with a lower software resolution are displayed using upscaling, and images with a higher software resolution are displayed using downscaling.

> #### Protip
> The vast majority of digital video content is available in at most `16:9 1080p` (branded as FHD), so higher resolutions such as `16:9 1440p` (branded as QHD) and `16:9 2160p` (branded as 4K) are largely pointless for content consumption.

#### Display Size
Pixel density (measured in pixels/dots per inch (PPI/DPI)), together with display size, determines whether a monitor looks crisp or blurry (affecting the clarity of text and images). With a standard fixed view distance, a small display with high pixel density will look crisp and a large display with low pixel density will look blurry. This is relevant because the choice of display size should not be arbitrary, but rather guided by a software imposed constraint: desktop software is designed for a fixed pixel density of 96 DPI (roughly).

Thus, for the standard `16:9 1080p` resolution, the ideal display size is between 22" - 24" (for larger displays, a higher resolution is needed to maintain crispness, which causes the software user interface elements to become too small and require scaling and manual adjustment).

#### Refresh Rate
The refresh rate is the frequency (measured in Hertz (`Hz`)) with which a monitor displays new images. Its software equivalent is framerate (measured in frames per second (fps)). The standard monitor frequency is `60Hz` (i.e. the monitor displays 60 images per second). Higher frequencies reduce input latency and have a greatly positive impact on responsiveness, provided the software can match them in framerate - if the computer can't produce new images (frames) quickly enough, old frames will be shown multiple times in a row, effectively wasting the increased refresh rate.

> #### Protip
> The vast majority of digital video content is available in either `30hz` (and `60hz`) for web content or `24hz` for cinematographic content; any refresh rate that is not an integer multiple of both will cause subtle frame pacing issues such as judder during panning scenes.

For interactive 3D applications such as games, where framerate fluctuates, things can be more problematic: if the refresh rate is not an exact integer multiple of the framerate, unpleasant stuttering can be perceived. To mitigate this, monitors can support a technology that interfaces with the operating system known as variable refresh rate ([VRR](https://en.wikipedia.org/wiki/Variable_refresh_rate) - branded as G-Sync for Nvidia and FreeSync for AMD) which allows the monitor refresh rate to synchronize with the application framerate.

#### Display Cable
Finally, the monitor's display cable must be compatible with the computer's graphics output (motherboard if using integrated graphics or graphics card if using dedicated graphics). The display connectivity standard for monitors is DisplayPort.

* * *

### Peripherals (Keyboard, Mouse, Controller, Webcam, Speakers, etc.)
Needless to say that RGB branded peripherals are uselessly more expensive than their "boring" counterparts with which perfectly neat and elegant setups can still be built - however, taste is subjective; anything works (as long as it's not a "gaming chair").
