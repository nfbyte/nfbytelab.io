# Refresh package repositories
apt-get update --assume-yes

# Update existing packages
apt-get upgrade --assume-yes

# Remove unused packages
apt-get autoremove --assume-yes

# Add the Docker package repository PGP key
curl https://download.docker.com/linux/ubuntu/gpg | apt-key add

# Add the Docker package repository
add-apt-repository \
	"deb https://download.docker.com/linux/ubuntu focal stable"

# Add the gVisor package repository PGP key
curl https://gvisor.dev/archive.key | apt-key add

# Add the gVisor package repository
add-apt-repository \
  "deb https://storage.googleapis.com/gvisor/releases release main"

# Add the ZeroTier package repository PGP key
curl https://download.zerotier.com/contact@zerotier.com.gpg | apt-key add

# Add the ZeroTier package repository
add-apt-repository \
  "deb https://download.zerotier.com/debian/focal focal main"

# Install packages
apt-get install --assume-yes \
  runsc \
  docker-ce \
  docker-compose-plugin \
  zerotier-one \
  openssh-server \
  zfsutils-linux

# Configure static IPv4 address
cat > /etc/netplan/01-netcfg.yaml << \
-----------------------------
network:
  version: 2
  renderer: networkd
  ethernets:
    $(ip address | awk '/inet.*brd/{print $NF;exit}'):
      dhcp4: no
      dhcp6: no
      addresses: [$(ip route get 1 | awk '{print $(NF-2);exit}')/24]
      gateway4: $(ip route | grep default | awk '{print $3}')
-----------------------------

# Load static IPv4 address configuration
netplan apply

# Configure DNS
cat > /etc/systemd/resolved.conf << \
----------------------------
[Resolve]
DNS=8.8.8.8
DNSStubListener=no
----------------------------

# Load DNS configuration
systemctl reload-or-restart systemd-resolved

# Fix DNS resolution
ln --symbolic --force \
  /run/systemd/resolve/resolv.conf /etc/resolv.conf

# Configure SSH
cat > /etc/ssh/sshd_config << \
----------------------------
PrintMotd no
DebianBanner no
LoginGraceTime 30s
MaxAuthTries 3
Protocol 2
UsePAM yes
ChallengeResponseAuthentication no
PermitRootLogin without-password
----------------------------

# Load SSH configuration
systemctl reload-or-restart sshd

# Configure Docker
cat > /etc/docker/daemon.json << \
----------------------------
{
    "live-restore": true,
    "default-runtime": "runsc",
    "runtimes": {
        "runsc": {
            "path": "/usr/bin/runsc",
            "runtimeArgs": [
                "--overlay2=none"
            ]
        }
    }
}
----------------------------

# Load Docker configuration
systemctl reload-or-restart docker

# Configure applications
cat > /etc/docker/compose.yml << \
----------------------------
services:
  AdGuard_Home:
    image: adguard/adguardhome
    restart: always
    network_mode: bridge
    ports:
      - 53:53/tcp
      - 53:53/udp
      - 3000:3000
    volumes:
      - /etc/AdGuard_Home:/opt/adguardhome/conf
  File_Browser:
    image: filebrowser/filebrowser
    command: --database /opt/database.db
    restart: always
    network_mode: bridge
    ports:
      - 4000:80
    volumes:
      - /Files:/srv
      - /etc/File_Browser:/opt
  Emby:
    image: emby/embyserver
    restart: always
    network_mode: bridge
    ports:
      - 7000:8096
    volumes:
      - /Files/Media:/mnt
      - /etc/Emby:/config
----------------------------

# Start applications
docker compose --file /etc/docker/compose.yml up --detach
